(ns post-min.components.dummy
  (:require [post-min.render-util :refer [defnc]]
            [helix.dom :as d]
            [post-min.gen.components.dummy-module :as css]))

(defnc dummy-comp []
  (d/p
   {:class css/dummy}
   "This is a p tag."))