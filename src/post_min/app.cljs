(ns post-min.app
  (:require ["react-dom/client" :refer [createRoot] :rename {createRoot create-root}]
            [helix.core :refer [$]]
            [helix.dom :as d]
            [post-min.components.dummy :refer [dummy-comp]]
            [post-min.render-util :refer [defnc]]))

(defnc app []
  (d/div
   (d/h1 "My Helix App")
   ($ dummy-comp)))

(defonce root (create-root (js/document.getElementById "root")))

(defn ^:dev/after-load mount-ui []
  (.render root ($ app)))

(defn ^:export init []
  (mount-ui))
